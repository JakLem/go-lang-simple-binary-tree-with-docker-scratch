FROM golang:latest as goCompiler

COPY app/main.go .

RUN CGO_ENABLED=0 go build main.go

FROM scratch

COPY --from=goCompiler /go/main /main

EXPOSE 80

ENTRYPOINT [ "/main" ]