package main

import (
	"fmt"
)

type Node struct {
	Key   int
	Left  *Node
	Right *Node
}

func (n *Node) InsertToTree(newKey int) {
	if n.Key < newKey {
		if n.Right == nil {
			n.Right = &Node{Key: newKey}
		} else {
			n.Right.InsertToTree(newKey)
		}
	} else if n.Key > newKey {
		if n.Left == nil {
			n.Left = &Node{Key: newKey}
		} else {
			n.Left.InsertToTree(newKey)
		}
	}
}

func (n *Node) SearchInTree(keyToFind int) bool {
	if n == nil {
		return false
	}

	if n.Key > keyToFind {
		return n.Left.SearchInTree(keyToFind)
	} else if n.Key < keyToFind {
		return n.Right.SearchInTree(keyToFind)
	}

	return true
}

func main() {
	fmt.Println("Start!")

	toInsert := [10]int{6, 10, 2, 25, 11, 125, 89, 22, 5, 999}
	toFind := [5]int{1, 2, 76, 6, 999}

	bTree := &Node{Key: 100}

	for _, v := range toInsert {
		bTree.InsertToTree(v)
	}

	for _, v := range toFind {
		fmt.Printf("Searching for %v : %v \n", v, bTree.SearchInTree(v))
	}

}
